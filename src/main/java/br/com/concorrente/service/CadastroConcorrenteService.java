package br.com.concorrente.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.concorrente.model.Concorrente;
import br.com.concorrente.model.Status;
import br.com.concorrente.repository.ConcorrenteRepository;
import br.com.concorrente.repository.filter.ConcorrenteFilter;

@Service
public class CadastroConcorrenteService {
	
	@Autowired
	private ConcorrenteRepository concorrentes;
	
	public void salvar(Concorrente concorrente) {
		try {
			concorrentes.save(concorrente);
		} catch (Error e) {
			throw new IllegalArgumentException("Erro ao salvar", e);
		}
	}
	
	public void excluir(Long codigo) {
		concorrentes.delete(codigo);
	}

	public String aprovarProposta(Long codigo) {
		LocalDate date = LocalDate.now();
		Concorrente concorrente = concorrentes.findOne(codigo);
		concorrente.setStatus(Status.APROVADO);
		concorrente.setDtAprovacao(date);
		concorrentes.save(concorrente);

		return Status.APROVADO.getDescricao();
	}

	public List<Concorrente> filtrar(ConcorrenteFilter filtro) {
		String descricao = filtro.getDescricao();
		return concorrentes.findByDescricaoLike(descricao);
	}

	public List<Concorrente> findAll() {
		return concorrentes.findAll();
	}
	public Concorrente findById(Long codigo) {
        Concorrente concorrente = concorrentes.findOne(codigo);
        return concorrente;
    }

}


