package br.com.concorrente.utils;

import java.io.PrintWriter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvException;

import br.com.concorrente.model.Concorrente;

public class WriteCsvToResponse {
	
	@Autowired

    private static final Logger LOGGER = LoggerFactory.getLogger(WriteCsvToResponse.class);

    public static void writeConcorrentes(PrintWriter writer, List<Concorrente> concorrentes)  {

        try {

            ColumnPositionMappingStrategy mapStrategy
                    = new ColumnPositionMappingStrategy();

            mapStrategy.setType(Concorrente.class);
            mapStrategy.generateHeader(mapStrategy);

            String[] columns = new String[]{"Código", "Descricao", "Status", "Telefone", "Cpf/Cnpj", "E-mail", "Ramo de Atividade", "Taxa Debito Concorrente", "Taxa Credito Concorrente", "Desconto Credito", "Desconto Debito"};
            mapStrategy.setColumnMapping(columns);

            StatefulBeanToCsv btcsv = new StatefulBeanToCsvBuilder(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .withMappingStrategy(mapStrategy)
                    .withSeparator(',')
                    .build();

            btcsv.write(concorrentes);
            

        } catch (CsvException ex) {

            LOGGER.error("Error mapping Bean to CSV", ex);
        }
    }

    public static void writeConcorrente(PrintWriter writer, Concorrente concorrente) {

        try {

            ColumnPositionMappingStrategy mapStrategy
                    = new ColumnPositionMappingStrategy();

            mapStrategy.setType(Concorrente.class);

            String[] columns = new String[]{"Código", "Descricao", "Status", "Telefone", "Cpf/Cnpj", "E-mail", "Ramo de Atividade", "Taxa Debito Concorrente", "Taxa Credito Concorrente", "Desconto Credito", "Desconto Debito"};
            mapStrategy.setColumnMapping(columns);

            StatefulBeanToCsv btcsv = new StatefulBeanToCsvBuilder(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .withMappingStrategy(mapStrategy)
                    .withSeparator(',')
                    .build();

            btcsv.write(concorrente);
            
           
            

        } catch (CsvException ex) {

            LOGGER.error("Error mapping Bean to CSV", ex);
        }
    }
}
