package br.com.concorrente.model;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;


@Entity
public class Concorrente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	
	@NotEmpty(message = "Descrição é obrigatória")
	@Size(max = 60, message = "A descrição não pode conter mais de 60 caracteres")
	private String descricao;
	

	@Enumerated(EnumType.STRING)
	private Status status;
	
	@NotNull(message="Telefone é obrigatório")
	private String telefone;
	
	@NotNull(message="O número de documento é obrigatório")
	private String cpfCnpj;
	
	private String email;
	
	private String ramoAtividade;
	
	private LocalDate dtAprovacao;
	
	
	public LocalDate getDtAprovacao() {
		return dtAprovacao;
	}

	public void setDtAprovacao(LocalDate dtAprovacao) {
		this.dtAprovacao = dtAprovacao;
	}



	@NotNull(message = "Valor de taxa é obrigatório")
	@DecimalMin(value = "0.01", message = "Valor não pode ser menor que 0,01")
	@NumberFormat(pattern = "#,##0.00")
	private Float txDebitoConcorrente;
	
	@NotNull(message = "Valor de taxa é obrigatório")
	@DecimalMin(value = "0.01", message = "Valor não pode ser menor que 0,01")
	@NumberFormat(pattern = "#,##0.00")
	private Float descontoDebito;
	
	@NotNull(message = "Valor de taxa é obrigatório")
	@DecimalMin(value = "0.01", message = "Valor não pode ser menor que 0,01")
	@NumberFormat(pattern = "#,##0.00")
	private Float txCreditoConcorrente;
	
	@NotNull(message = "Valor de taxa é obrigatório")
	@DecimalMin(value = "0.01", message = "Valor não pode ser menor que 0,01")
	@NumberFormat(pattern = "#,##0.00")
	private Float descontoCredito;
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRamoAtividade() {
		return ramoAtividade;
	}

	public void setRamoAtividade(String ramoAtividade) {
		this.ramoAtividade = ramoAtividade;
	}

	public Float getTxDebitoConcorrente() {
		return txDebitoConcorrente;
	}

	public void setTxDebitoConcorrente(Float txDebitoConcorrente) {
		this.txDebitoConcorrente = txDebitoConcorrente;
	}

	public Float getDescontoDebito() {
		return descontoDebito;
	}

	public void setDescontoDebito(Float descontoDebito) {
		this.descontoDebito = descontoDebito;
	}

	public Float getTxCreditoConcorrente() {
		return txCreditoConcorrente;
	}

	public void setTxCreditoConcorrente(Float txCreditoConcorrente) {
		this.txCreditoConcorrente = txCreditoConcorrente;
	}

	public Float getDescontoCredito() {
		return descontoCredito;
	}

	public void setDescontoCredito(Float descontoCredito) {
		this.descontoCredito = descontoCredito;
	}
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
