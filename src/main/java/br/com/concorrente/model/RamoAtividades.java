package br.com.concorrente.model;

public enum RamoAtividades {
	Varejo("Varejo",2.5, 2.5),
	Atacado("Atacado", 2.0, 2.5);
	
	
	private Double taxaCreditoMinima;
	private Double TaxaDebitoMinima;
	private String descricao;
	
	
	 RamoAtividades(String descricao, Double taxaCreditoMinima, Double taxaDexitoMinima){
         this.descricao = descricao;
         this.taxaCreditoMinima = taxaCreditoMinima;
         this.TaxaDebitoMinima = taxaDexitoMinima;
     }
	

	

	public Double getTaxaCreditoMinima() {
		return taxaCreditoMinima;
	}

	public void setTaxaCreditoMinima(Double taxaCreditoMinima) {
		this.taxaCreditoMinima = taxaCreditoMinima;
	}

	public Double getTaxaDebitoMinima() {
		return TaxaDebitoMinima;
	}

	public void setTaxaDebitoMinima(Double taxaDebitoMinima) {
		TaxaDebitoMinima = taxaDebitoMinima;
	}

	RamoAtividades(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}

