package br.com.concorrente.model;

public enum Status {

	PENDENTE("Pendente"), APROVADO("Aprovado"), REPROVADO("Reprovado");

	private String descricao;

	Status(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
