package br.com.concorrente.repository.filter;

public class ConcorrenteFilter {
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
