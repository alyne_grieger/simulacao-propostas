package br.com.concorrente.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.concorrente.model.Concorrente;

public interface ConcorrenteRepository extends JpaRepository<Concorrente, Long>{
	public List<Concorrente> findByDescricaoLike(String descricao);
}