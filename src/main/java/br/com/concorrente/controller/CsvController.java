package br.com.concorrente.controller;


import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.concorrente.model.Concorrente;
import br.com.concorrente.service.CadastroConcorrenteService;
import br.com.concorrente.utils.WriteCsvToResponse;

@RestController
public class CsvController {

    @Autowired
    CadastroConcorrenteService cadastroService;

    @RequestMapping(value = "/export", produces = "text/csv")
    public void findConcorrentes(HttpServletResponse response) throws IOException {

        List<Concorrente> concorrentes = (List<Concorrente>) cadastroService.findAll();

        WriteCsvToResponse.writeConcorrentes(response.getWriter(), concorrentes);
    }

    @RequestMapping(value = "/export/{codigo}", produces = "text/csv")
    public void findConcorrente(@PathVariable Long codigo, HttpServletResponse response) throws IOException {

        Concorrente concorrente = cadastroService.findById(codigo);
        WriteCsvToResponse.writeConcorrente(response.getWriter(), concorrente);
    }
    
    

}