package br.com.concorrente.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

import br.com.concorrente.model.Concorrente;
import br.com.concorrente.model.RamoAtividades;
import br.com.concorrente.model.Status;
import br.com.concorrente.repository.filter.ConcorrenteFilter;
import br.com.concorrente.service.CadastroConcorrenteService;


@Controller
@RequestMapping("/concorrente")
public class ConcorrenteController {

	private static final String CADASTRO_VIEW = "CadastroConcorrente";
	private static final String PESQUISA_VIEW = "PesquisaConcorrentes";
	
	@Autowired
	private CadastroConcorrenteService cadastroConcorrenteService;

	@RequestMapping("/novo")
	public ModelAndView novo() {
		ModelAndView mv = new ModelAndView(CADASTRO_VIEW);
		mv.addObject(new Concorrente());
		return mv;
	}
	
	@RequestMapping(value="/export-propostas", method = RequestMethod.GET)
    public void exportCSV(HttpServletResponse response) throws Exception {

        String filename = "propostas.csv";

        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + filename + "\"");

        StatefulBeanToCsv<Concorrente> writer = new StatefulBeanToCsvBuilder<Concorrente>(response.getWriter())
                .withQuotechar(CSVWriter.DEFAULT_SEPARATOR)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withOrderedResults(false)
                .build();
        
        writer.write(cadastroConcorrenteService.findAll());
				
    }


	@RequestMapping(method = RequestMethod.POST)
	public String salvar(@Validated Concorrente concorrente, Errors errors, RedirectAttributes attributes) {
		if (errors.hasErrors()) {
			return CADASTRO_VIEW;
		}

		try {
			cadastroConcorrenteService.salvar(concorrente);
			attributes.addFlashAttribute("mensagem", "Concorrente salvo com sucesso!");
			return "redirect:/concorrente/novo";
		} catch (IllegalArgumentException e) {
			errors.rejectValue(null, e.getMessage());
			return CADASTRO_VIEW;
		}
	}

	@RequestMapping
	public ModelAndView pesquisar(@ModelAttribute("filtro") ConcorrenteFilter filtro) {
		List<Concorrente> todosConcorrentes = filtro == null || filtro.getDescricao() == null
				? cadastroConcorrenteService.findAll()
				: cadastroConcorrenteService.filtrar(filtro);

		ModelAndView mv = new ModelAndView(PESQUISA_VIEW);
		mv.addObject("concorrentes", todosConcorrentes);
		return mv;
	}

	@RequestMapping("{codigo}")
	public ModelAndView edicao(@PathVariable("codigo") Concorrente concorrente) {
		ModelAndView mv = new ModelAndView(CADASTRO_VIEW);
		mv.addObject(concorrente);
		return mv;
	}

	@RequestMapping(value = "{codigo}", method = RequestMethod.DELETE)
	public String excluir(@PathVariable Long codigo, RedirectAttributes attributes) {
		cadastroConcorrenteService.excluir(codigo);

		attributes.addFlashAttribute("mensagem", "Concorrente excluído com sucesso!");
		return "redirect:/concorrente";
	}

	@RequestMapping(value = "/{codigo}/aprovar", method = RequestMethod.PUT)
	public @ResponseBody String aprovarProposta(@PathVariable Long codigo) {
		return cadastroConcorrenteService.aprovarProposta(codigo);
	}

	@ModelAttribute("todosStatus")
	public List<Status> todosStatus() {
		return Arrays.asList(Status.values());
	}
	@ModelAttribute("ramosAtividades")
	public List<RamoAtividades> ramosAtividades() {
		return Arrays.asList(RamoAtividades.values());
	}

}
